---
title: Hexo博客建立（薅羊毛）
date: 2022/5/19
comments: true
categories: [Hexo]
tags: [标签]

---

## Hexo是个
Hexo 是一个快速、简洁且高效的博客框架。Hexo 使用 Markdown（或其他渲染引擎）解析文章，在几秒内，即可利用靓丽的主题生成静态网页。

在这篇文章中，我们将从如何安装开始，实现Hexo博客平台搭建，写出第一篇文章，了解 GitLab Runner 并借助GitLab将博客部署到个人仓库以实现浏览器访问，申请自己的域名及https证书，最后将博客地址提交到搜索引擎以让大家能够搜索到你。



## 准备工作
安装 Hexo 相当简单，只需要在自己对应的平台先安装下列应用程序即可：

1.[Node.js](https://nodejs.org/zh-cn/download/) (Node.js 版本需不低于 10.13，建议使用 Node.js 12.0 及以上版本)
Node.js 是一个开源和跨平台的 JavaScript 运行时环境。
2.[Git](https://git-scm.com/downloads)
Git是一个开源的分布式版本控制系统，可以有效、高速地处理从很小到非常大的项目版本管理。
<!-- （其实很简单，只需要无脑Next即可） -->

Hexo依赖于Node.js执行环境及其npm包管理器，同时也需要Git来拉取资源，若完成了上述软件的安装，即可进行下一步。

当然，你也可以去查看一下官方的[说明文档](https://hexo.io/zh-cn/docs/)

## 安装Hexo
很简单，安装Hexo只需要一条命令即可，而它被显示在了[Hexo主页](https://hexo.io/zh-cn/)最显眼的位置

```bash
1 | npm install hexo-cli -g
```

例如在Windows下，打开命令提示符cmd或powershell终端并执行安装命令

![安装Hexo-Windows](../First_of_Hexo/img/安装Hexo-Windows.png)
Linux 下也是同样的操作，但是如果不想面对满屏的<font color="#dd0000">ERROR</font> ，记得加上sudo或以root身份执行
![安装Hexo-Linux](../First_of_Hexo/img/安装Hexo-Linux.png)
如果正确安装，通过命令`hexo version`可以查看版本号。

## 初始化&运行
如果已经安装好了Hexo，我们只需要敲击简单的命令行即可初始化博客，不过首先我们需要找一个用于放置博客的文件夹，注意这个文件夹**必须是空的**，这里我新建了一个名为`test`的文件夹，然后我们cd到这个空文件夹内（或者在这个位置打开终端,git也可以），执行如下命令以初始化Hexo
```bash
1 | hexo init
```
如果使用的终端是`Windows PowerShell`的话可能会出现<font color="#dd0000">“无法加载文件，因为在此系统上禁止运行脚本”</font>的报错，这是因为 `PowerShell`默认执行策略是禁止脚本命令运行的，可以敲击cmd进入命令提示符再执行初始化，或者以管理员身份打开`PowerShell`的同时执行`Set-ExecutionPolicy RemoteSigned`，将`Powershell`的执行策略更改为`RemoteSigned`

![Hexo初始化](../First_of_Hexo/img/Hexo初始化.png)
看不明白？这里我们逐行翻译一下大意，第一行表明Hexo正在从`https://github.com/hexojs/hexo-starter.git`这个地址获取Hexo启动器相关文件，而第二行则提示无法从这个地址获取文件，原因是无法解析`GitHub.com`,于是第三行提示将从本地安装的Hexo文件中复制一份启动器到当前目录，第四行安装依赖文件，第五行提示安装成功。

考虑到国内GitHub的确经常抽风，这是正常现象，看到这个高亮的 **<font color="#dddd00">WARN</font>** 不要紧张，当然不排除网络质量好的时候能直接从云端获取资源。

到这里，你会发现文件夹里面已经出现了很多东西：
![Hexo文件夹](../First_of_Hexo/img/Hexo文件夹.png)
接下来，在刚才的终端内输入启动命令：
```bash
1 | hexo s
```
![Hexo运行](../First_of_Hexo/img/Hexo运行.png)
这时候按住Ctrl点击Hexo启动器给出的连接，即可在本地访问渲染后的博客页面：
![Hexo默认主题页面](../First_of_Hexo/img/Hexo默认主题页面.png)
当想结束渲染服务时，按下Ctrl+C退出渲染器。

到了这一步，如果恰好你有一台服务器、并且恰好拥有公网IP，或者有那么一些关于隧道的知识，你已经可以通过上述这些方式将预览地址发布出去供他人访问了。

